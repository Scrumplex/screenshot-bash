{
  description = "screnshot-bash";

  inputs.nixpkgs.url = "github:NixOS/nixpkgs/nixpkgs-unstable";

  outputs = {
    self,
    nixpkgs,
  }: let
    # User-friendly version number.
    version = builtins.substring 0 8 self.lastModifiedDate;

    supportedSystems = ["x86_64-linux"];
    forAllSystems = nixpkgs.lib.genAttrs supportedSystems;

    pkgs = forAllSystems (system: nixpkgs.legacyPackages.${system});

    packagesFn = pkgs: {
      screenshot-bash = pkgs.callPackage ./nix {inherit version self;};
    };
  in {
    packages = forAllSystems (system: let
      packages = packagesFn pkgs.${system};
    in
      packages // {default = packages.screenshot-bash;});

    overlays = let
      overlay = final: packagesFn;
    in {
      screenshot-bash = overlay;
      default = overlay;
    };
  };
}
