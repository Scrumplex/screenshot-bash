{ lib
, stdenv
, makeWrapper
, bash
, coreutils
, curl
, file
, gzip
, kdialog
, sox
, xdg-utils

  # flake
, self
, version
}:

let
  prefixPath = programs:
    "--prefix PATH ':' '${lib.makeBinPath programs}'";
  programs = [
    bash
    coreutils
    curl
    file
    gzip
    kdialog
    sox
    xdg-utils
  ];
in stdenv.mkDerivation {
  pname = "screenshot-bash";
  inherit version;

  src = lib.cleanSource self;

  nativeBuildInputs = [ makeWrapper ];

  installPhase = ''
    mkdir -p $out/bin
    cp screenshot-bash upload-bash $out/bin/
    wrapProgram $out/bin/screenshot-bash --prefix PATH : $out/bin ${prefixPath programs}
    wrapProgram $out/bin/upload-bash --prefix PATH : $out/bin  ${prefixPath programs}
  '';

  meta = with lib; {
    homepage = "https://codeberg.org/Scrumplex/screenshot-bash";
    description = "screenshot - upload - copy-url pipeline";
    longDescription = ''
      Make screenshots and upload them to your server! This script intends to provide an alternative to ShareX for Linux.
    '';
    platforms = platforms.linux;
    license = licenses.gpl3Plus;
    maintainers = with maintainers; [ Scrumplex ];
  };
}
