screenshot-bash
---------------
screenshot - upload - copy-url pipeline

Make screenshots and upload them to your server! This script intends to provide
an alternative to [ShareX](https://github.com/ShareX/ShareX) for Linux.

With the modular design, screenshot-bash can be adapted to be used on any
desktop environment, window manager and display protocol.

Apart from creating screenshots, uploading them and copying the URL,
`screenshot-bash` does not offer any advanced features such as ShareX.

# Installation
Arch Linux Package: [screenshot-bash](https://aur.archlinux.org/packages/screenshot-bash/)<sup>AUR</sup>

## Manual installation
 - Install [an endpoint](#endpoints)
 - Install dependencies
   - Arch Linux: `pacman -S --needed coreutils bash gzip sox xdg-utils curl kdialog file`
 - Install tools of choice
   - By default screenshot-bash uses `grim`, `slurp` and `wl-copy`
     - Arch Linux: `pacman -S --needed grim slurp wl-copy`
 - Download a release of this repository
 - Put `screenshot-bash` and `upload-bash` somewhere in your `$PATH`
 - Customize the `screenshot-bash.conf` file to match your needs and place it
   at `$HOME/.config/screenshot-bash.conf` or `$XDG_CONFIG_HOME/screenshot-bash.conf`
 - Configure your DE/WM to run the script (`screenshot-bash`) with a hotkey

# TODOs
- [ ] Make xdg-open replacable/configurable
- [ ] Allow user to modify pipeline and pipeline order
  - [ ] Also allow custom steps, defined in configuration

# License
This project is licensed under the GNU General Public License v3.
You can find more information about it in the [LICENSE](LICENSE) file.
